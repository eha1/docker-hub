# Desafios Zello
## SRE/DevOps

Desafio para a primeira fase de testes de pessoas candidatas às vagas de **Site Reliability Engineer / DevOps**.

Este desafio é válido tanto para junior, quanto para pleno ou sênior, porém os critérios de avaliação vão variar para cada um dos níveis de experiência.

* 1. Conhecimentos, habilidades e tecnologias necessárias
* 2. Premissas
* 3. Descrição do desafio
* 4. Como submeter o desafio para análise

### 1. Conhecimentos, habilidades e tecnologias necessárias

* [SRE](https://sre.google/)
* Linux
* Shell Script
* [NestJS](https://nestjs.com/)
* [SpringBoot](https://spring.io/projects/spring-boot)
* [Docker](https://www.docker.com/)
* [Docker Compose](https://docs.docker.com/compose/)
* [Kubernetes](https://kubernetes.io/)
* [Git](https://git-scm.com/)
* [GitFlow](https://www.atlassian.com/br/git/tutorials/comparing-workflows/gitflow-workflow)
* [Gitlab CI](https://docs.gitlab.com/ce/ci/)

### 2. Premissas

* Os fontes não devem ser alterados;
* As dúvidas, sugestões e problemas devem ser registrados [aqui](https://gitlab.zello.services/publico/challenges/devops/-/issues) e estarão disponíveis para todos, assim como as respostas;
* Você pode utilizar o [minikube](https://kubernetes.io/pt-br/docs/tutorials/hello-minikube/) na <b>Etapa 4</b>

### 3. Descrição do desafio

<b>Etapa 1:</b>
* Criar um contêiner com o banco de dados MongoDB; 
* Criar um contêiner com a aplicação NestJs disponivel em "fontes/frontend"; 
* Criar um contêiner com a aplicação SpringBoot disponivel em "fontes/backend"; 

_**Entregável:**_ Os Dockerfiles

<b>Etapa 2:</b>
* Criar um pipeline que irá construir e registrar os contêineres na sua conta pessoal no Docker Hub a cada _**push**_ 

_**Entregável:**_ O gitlab-ci.yml

<b>Etapa 3:</b>
* Executar os contêineres via comando "docker-compose up"

_**Entregável:**_ O docker-compose.yml

<b>Etapa 4:<b>
* Colocar a aplicação para funcionar no Kubernetes, no namespace "challenge";

_**Entregável:**_ Os objetos yaml - deployment, service, volume, volumeclaim, ingress - e os comandos kubectl

### 4. Como submeter o desafio para análise

* Basta compactar os entregáveis em um arquivo zip, e selecioná-lo no campo adquado do formulário abaixo, preenchendo também as demais informações.

[Formulário de Envio do Desafio](https://bit.ly/DevOpsZello)

* Submeta o formulário a data limite, mesmo que não tenha completado todas as etapas.

* As pessoas candidatas que forem aprovadas nesta etapa, serão convidadas para uma entrevista técnica onde terão a oportunidade de apresentar o resultado do desafio e responder eventuais dúvidas das pessoas avaliadoras.
